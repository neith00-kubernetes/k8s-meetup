#!/bin/bash
# Author neith00
TERRAFORM_RELEASE='0.11.10'
KUBESPRAY_URL=https://github.com/neith00/kubespray.git
KUBESPRAY_TAG=terraformFix
ARCH=$(uname -p)
ROOTDIR=$(pwd)
export PATH=$PATH:$ROOTDIR/bin

function check_terraform_version {
    command -v terraform
    RETVAL=$?
    if [[ $RETVAL != 0 ]] ; then
        return 1
    else
        VERSION=$(terraform version|head -n 1|cut -d 'v' -f 2)
        if [[ "$VERSION" != "$TERRAFORM_RELEASE" ]]; then
            echo "Wrong Terraform version"
	    return 1
        fi
    fi
    return 0
}

function setup_kubespray {
    cd "$ROOTDIR" || return
    if [ -d kubespray ]; then
        cd kubespray && git pull && git checkout "$KUBESPRAY_TAG"
    else
        git clone -b "$KUBESPRAY_TAG" --single-branch --depth 1 "$KUBESPRAY_URL"
    fi
}

function get_terraform {
    TERRAFORM_URL=https://releases.hashicorp.com/terraform/"$TERRAFORM_RELEASE"/terraform_"$TERRAFORM_RELEASE"_linux_"$TERRA_ARCH".zip
    WORKDIR=$(mktemp -d)
    cd "$WORKDIR" && curl "$TERRAFORM_URL" -o terraform.zip \
    && unzip terraform.zip && mkdir $ROOTDIR/bin \
    && mv terraform $ROOTDIR/bin/terraform \
    && rm -rf "$WORKDIR" 
}

case "$ARCH" in
"x86_64")
    TERRA_ARCH='amd64' 
    ;;
"2")
    TERRA_ARCH='386' 
    ;;
*)
    echo "Unknown arch"
    exit 1
    ;;
esac

check_terraform_version
RETVAL=$?

if [[ $RETVAL != 0 ]] ; then
	get_terraform
fi

setup_kubespray
