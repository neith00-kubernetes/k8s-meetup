#!/bin/bash
grep '18.04' /etc/lsb-release || echo "this script support only ubuntu 18.04"
sudo apt-get update \
&& sudo apt install unzip python3.7 python3.7-dev python3-pip \
&& sudo -H pip3 install pipenv
